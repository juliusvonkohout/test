# Please note, that the next FROM line shall be replaced by the BuildConfig.
FROM registry.access.redhat.com/rhscl/python-36-rhel7

# switch to user root so that we can use yum to install dependencies, etc.
USER root
RUN yum update -y
RUN yum install -y file readline readline-devel bzip2-devel
# switch back to default user
USER 1001